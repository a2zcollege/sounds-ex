
var audioCtx = new AudioContext()  // create web audio api context
var oscillator = audioCtx.createOscillator(); // create Oscillator node

//var gainNode = audioCtx.createGain();


oscillator.type = 'sine';
oscillator.frequency.value = 130; // value in hertz
oscillator.connect(audioCtx.destination);

//gainNode.connecet(audioCtx.destination);
//gainNode.gain.value = 0.5;

oscillator.start();
oscillator.stop(1);
